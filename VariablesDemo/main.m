//
//  main.m
//  VariablesDemo
//
//  Created by James Cash on 01-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        NSInteger foo = 1;

        NSLog(@"foo is %ld", foo);

        foo = 2;

        foo = foo / 2;

        BOOL isThing = YES;

        if (isThing) {
            NSLog(@"Yes, thing");
            isThing = NO;
        }

        BOOL isFooBig = foo > 10;
        if (isFooBig) {
            NSLog(@"Foo is big");
        } else {
            NSLog(@"foo is small");
        }

        CGFloat bar = 7.05;
        bar = foo * 10.0 / 17.0;
        NSLog(@"bar = %g", bar);
        foo = bar;
        NSLog(@"foo = %ld", foo);
        NSLog(@"int div: 5 / 2 = %d", 5 / 2);
        NSLog(@"float div: 5 / 2.0 = %g", 5 / 2.0);


        // Value types
        NSInteger foobar = 1;
        NSInteger bazquux = 2;
        NSLog(@"foobar = %ld bazquux = %ld", foobar, bazquux);
        foobar = bazquux;
        NSLog(@"foobar = %ld bazquux = %ld", foobar, bazquux);
        bazquux = 5;
        NSLog(@"foobar = %ld bazquux = %ld", foobar, bazquux);

//        BOOL a, b, c;

        NSMutableString *quux = [NSMutableString stringWithString:@"hello world"];
        NSString *snth = @"hello world";
        NSString *aoeu = quux;
        NSLog(@"quux: %@", quux);

        [quux appendString:@" bloop"]; // this changes aoeu!

        // Scopes
        {
            NSInteger count = 11;
            BOOL countIsGreaterThanTen = NO;
            if (count > 10) {
                countIsGreaterThanTen = YES;
            }

            if (countIsGreaterThanTen) {
                NSLog(@"The count really is more than ten");
            }
        }

        NSString *bloop = @"quote: \"aoeu\"!";

    }
    return 0;
}
